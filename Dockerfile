FROM forumi0721/alpine-armhf-base:latest

MAINTAINER ozolli <ozolli@gmail.com>

COPY docker-bin/. /usr/local/bin/

RUN ["docker-build-start"]

RUN ["docker-init"]

RUN ["docker-build-end"]

ENTRYPOINT ["docker-run"]

EXPOSE 80/tcp 3306/tcp
